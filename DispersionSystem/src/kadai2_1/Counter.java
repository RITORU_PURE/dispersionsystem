package kadai2_1;

public class Counter {
	private int count;
	
	public synchronized void countUp(int clientId){
		if((clientId % 2) == (count % 2)){
			count++;
			System.out.println(count + " ID:" + clientId);
			notifyAll();
		}else{
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		}
	}
	
	public Counter(){
		count = 0;
	}
	
}
